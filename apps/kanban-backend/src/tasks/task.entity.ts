import { Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn } from 'typeorm';
import { Category } from '../categories/category.entity';

@Entity()
export class Task {
    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    title: string;

    @Column()
    desc: string;

    @ManyToOne(() => Category, category => category.tasks, {onUpdate: 'CASCADE', onDelete: 'CASCADE'})
    @JoinColumn()
    category: Category;
}