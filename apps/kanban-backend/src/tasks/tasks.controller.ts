import { Controller, Get } from '@nestjs/common';
import { Task } from './task.entity';
import { TasksService } from './tasks.service';
import { Post,Put, Delete, Body, Param } from  '@nestjs/common';

@Controller('tasks')
export class TasksController {
    constructor(private tasksService: TasksService){}

    @Get()
    index(): Promise<Task[]> {
      return this.tasksService.findAll();
    }

    @Post('')
    async create(@Body() taskData: Task): Promise<any> {
      return this.tasksService.create(taskData);
    }

    @Get(':id')
    getOne(@Param('id') id): Promise<any> {
        return this.tasksService.findById(id)
    }

    @Put(':id')
    async update(@Param('id') id, @Body() taskData: Task): Promise<any> {
        taskData.id = Number(id);
        return this.tasksService.update(taskData);
    }

    @Delete(':id')
    async delete(@Param('id') id): Promise<any> {
      return this.tasksService.delete(id);
    }
}