import { Controller, Get } from '@nestjs/common';
import { Category } from './category.entity';
import { CategoriesService } from './categories.service';
import { Post, Put, Delete, Body, Param } from '@nestjs/common';

@Controller('categories')
export class CategoriesController {
  constructor(private categoriesService: CategoriesService) {}

  @Get()
  index(): Promise<Category[]> {
    return this.categoriesService.findAll();
  }

  @Post('')
  async create(@Body() categoryData: Category): Promise<any> {
    return this.categoriesService.create(categoryData);
  }

  @Put(':id')
  async update(@Param('id') id, @Body() categoryData: Category): Promise<any> {
    categoryData.id = Number(id);
    return this.categoriesService.update(categoryData);
  }

  @Delete(':id')
  async delete(@Param('id') id): Promise<any> {
    return this.categoriesService.delete(id);
  }
}
