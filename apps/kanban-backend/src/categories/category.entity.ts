import { Entity, Column, PrimaryGeneratedColumn, OneToMany, JoinColumn } from 'typeorm';
import { Task } from '../tasks/task.entity';

@Entity()
export class Category {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  title: string;

  @OneToMany((type) => Task, (task) => task.category, {
    cascade: true
  })
  @JoinColumn()
  tasks: Task[];
}
