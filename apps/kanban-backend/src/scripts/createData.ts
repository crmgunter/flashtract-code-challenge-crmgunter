import { Connection, getRepository } from "typeorm";
import { Category } from "../categories/category.entity";
import { Task } from "../tasks/task.entity";

export const createCategories = async (con: Connection) => {
    const categoryRepository = await getRepository(Category)
    const categoryCount = await categoryRepository.find()

    if (categoryCount.length < 1) {
        const category = new Category()
        category.title = "Backlog"
    
        
        const task = new Task()
        task.title = "Click me!"
        task.desc = "Hi! Feel free to have a look around. You can delete this modal with the trash icon, or close it by clicking outside of the modal or any of the close options. If you click this text, you can edit the description. Create some categories and tasks, and try dragging tasks around!"
        task.category = category
    
        
        
        await con.manager.save(category) as Category
        await con.manager.save(task) as Task
    }
}