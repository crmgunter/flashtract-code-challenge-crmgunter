import { Module } from '@nestjs/common';

import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule} from "@nestjs/typeorm";
import { TasksModule } from '../tasks/tasks.module';
import { CategoriesModule } from '../categories/categories.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      entities: ['src/**/*.entity.ts'],
      autoLoadEntities: true,
      synchronize: true,
      database: 'kanban-db.sqlite3',
      type: 'sqlite'
    }),
    CategoriesModule,
    TasksModule
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
