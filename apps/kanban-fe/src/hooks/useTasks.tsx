import {useMutation, useQueryClient} from 'react-query';
import axios from 'axios';

export const deleteItem = (id: number) => {
    return axios.delete(`http://localhost:3333/api/categories/${id}`)
}

const createTask = (taskData:{title: string, desc: string, category: number}) => {
    return axios.post("http://localhost:3333/api/tasks", taskData)
}

const updateTask = (taskData:{id: number, category: number}) => {
    return axios.put(`http://localhost:3333/api/tasks/${taskData.id}`, taskData)
}

const updateTaskDescription = (taskData:{id: number, title: string, desc: string}) => {
    return axios.put(`http://localhost:3333/api/tasks/${taskData.id}`, taskData)
}

const deleteTask = (id: number) => {
    return axios.delete(`http://localhost:3333/api/tasks/${id}`)
}

export const useCreateTask = () => {
    const queryClient = useQueryClient()
    return useMutation(createTask, {
        onSuccess: () => {
            queryClient.invalidateQueries('categories')
        }
    })
}

export const useUpdateTask = () => {
    const queryClient = useQueryClient()
    return useMutation(updateTask, {
        onSuccess: () => {
            queryClient.invalidateQueries('categories')
        }
    })
}

export const useUpdateTaskDescription = () => {
    const queryClient = useQueryClient()
    return useMutation(updateTaskDescription, {
        onSuccess: () => {
            queryClient.invalidateQueries('categories')
        }
    })
}

export const useDeleteTask = () => {
    const queryClient = useQueryClient()
    return useMutation(deleteTask, {
        onSuccess: () => {
            queryClient.invalidateQueries('categories')
        }
    })
}