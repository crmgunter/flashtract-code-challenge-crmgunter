import {useQuery, useMutation, useQueryClient} from 'react-query';
import axios from 'axios';

// example of generic get query
// dynamic so you could query for any endpoint here if desired
export const useGetCategories = (description: string, endpoint: string) => {
    const {isLoading, error, data} = useQuery(description, () => axios(endpoint))
    return {isLoading, error, data};
}

export const deleteItem = (id: number) => {
    return axios.delete(`http://localhost:3333/api/categories/${id}`)
}

const createCategory = (categoryData:{title: string}) => {
    return axios.post("http://localhost:3333/api/categories", categoryData)
}

const deleteCategory = (id: number) => {
    return axios.delete(`http://localhost:3333/api/categories/${id}`)
}

export const useCreateCategory = () => {
    const queryClient = useQueryClient()
    return useMutation(createCategory, {
        onSuccess: () => {
            queryClient.invalidateQueries('categories')
        }
    })
}

export const useDeleteCategory = () => {
    const queryClient = useQueryClient()
    return useMutation(deleteCategory, {
        onSuccess: () => {
            queryClient.invalidateQueries('categories')
        }
    })
}