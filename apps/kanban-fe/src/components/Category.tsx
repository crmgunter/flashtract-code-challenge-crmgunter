import {DeleteIcon} from '@chakra-ui/icons';
import {Heading, Box, IconButton} from '@chakra-ui/react';
import {FC} from 'react';
import {useDeleteCategory} from '../hooks/useCategories';
import AddTaskModal from './AddTaskModal';
import Task from './Task';

type CategoryType = {
    id: number;
    title: string;
    tasks: [];
};

interface CategoryProps {
    category: CategoryType;
}

const Category: FC<CategoryProps> = ({category}) => {
    const {mutate} = useDeleteCategory();

    return (
        <Box bg="#335" padding={'15px'} textColor={'white'} width={['50vw', '50vw', '40vw', '20vw']} height="100%" display="flex" alignItems={'center'} flexDirection={'column'} borderRadius={'8px'}>
            <Box display={'flex'} justifyContent={'space-between'} alignItems={'center'} width={'100%'} mb={'15px'}>
                <Heading as={'h2'} size="lg" whiteSpace={'nowrap'} overflow={'hidden'} textOverflow={'ellipsis'}>
                    {category.title}
                </Heading>
                <AddTaskModal category={category} />
            </Box>
            <Box width="100%" overflowX={'scroll'} height={'100%'} display="flex" justifyContent={'flex-start'} flexDirection={'column'} alignItems="flex-start">
                {category.tasks.map((task: {id: number; title: string; desc: string}) => (
                    <Task key={task.id} task={task} />
                ))}
            </Box>
            <Box alignSelf="flex-start">
                <IconButton colorScheme="red" aria-label="Delete task" onClick={() => mutate(category.id)} icon={<DeleteIcon />} />
            </Box>
        </Box>
    );
};

export default Category;
