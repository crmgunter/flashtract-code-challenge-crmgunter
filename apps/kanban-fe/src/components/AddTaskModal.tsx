import {FormControl, FormLabel, FormErrorMessage, Button, Input, Textarea, Box, useDisclosure, Heading, IconButton, Modal, ModalOverlay, ModalContent, ModalHeader, ModalFooter, ModalBody, ModalCloseButton} from '@chakra-ui/react';
import {Formik, Form, Field} from 'formik';
import {useCreateTask} from '../hooks/useTasks';
import {AddIcon} from '@chakra-ui/icons';
import {FC, useRef} from 'react';

type CategoryType = {
    id: number;
};

interface CategoryProps {
    category: CategoryType;
}

const AddTaskModal: FC<CategoryProps> = ({category}) => {
    const {mutate} = useCreateTask();
    const {isOpen, onOpen, onClose} = useDisclosure();
    const initialRef = useRef<HTMLInputElement>(null)

    const handleSubmit = (values: {title: string; desc: string; category: number}) => {
        mutate(values);
        onClose();
    };

    return (
        <Box>
            <IconButton colorScheme="green" aria-label="Add task" onClick={onOpen} icon={<AddIcon />} />

            <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent minWidth={'50vw'} minHeight={'50vh'}>
                    <ModalHeader>
                        <Heading size="xl">Add task</Heading>
                    </ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Box width="100%" display="flex" justifyContent={'center'}>
                            <Formik
                                initialValues={{title: '', desc: '', category: category.id}}
                                onSubmit={(values, actions) => {
                                    values.category = category.id;
                                    values.title = values.title.trim();
                                    values.desc = values.desc.trim();

                                    handleSubmit(values);
                                    actions.resetForm();
                                }}>
                                {(props) => (
                                    <Form style={{width: '100%'}}>
                                        <Box display={'flex'} flexDirection="column" justifyContent="center" alignItems={'flex-end'}>
                                            <Field name="title">
                                                {(props: any) => (
                                                    <FormControl isInvalid={props.form.errors.title && props.form.touched.title}>
                                                        <FormLabel htmlFor="title">
                                                            <Heading size="lg">Title</Heading>
                                                        </FormLabel>
                                                        <Input ref={initialRef} fontSize={'2xl'} width="100%" required {...props.field} id="title" placeholder="Enter a title" />
                                                        <FormErrorMessage>{props.form.errors.category}</FormErrorMessage>
                                                    </FormControl>
                                                )}
                                            </Field>

                                            <Field name="desc">
                                                {(props: any) => (
                                                    <FormControl isInvalid={props.form.errors.desc && props.form.touched.desc}>
                                                        <FormLabel mt={'5'} htmlFor="desc">
                                                            <Heading size="lg">Description</Heading>
                                                        </FormLabel>
                                                        <Textarea fontSize={'2xl'} resize="none" minHeight={'20vw'} maxHeight={'60vw'} {...props.field} id="desc" placeholder="Please enter a description" />
                                                        <FormErrorMessage>{props.form.errors.desc}</FormErrorMessage>
                                                    </FormControl>
                                                )}
                                            </Field>
                                            <Button alignSelf={'center'} mt={'5'} type="submit">
                                                Submit
                                            </Button>
                                        </Box>
                                    </Form>
                                )}
                            </Formik>
                        </Box>
                    </ModalBody>

                    <ModalFooter display={'flex'} justifyContent={'space-between'}>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </Box>
    );
};

export default AddTaskModal;
