import {Box, Text, Textarea, Button, IconButton, Input, Heading, Modal, ModalOverlay, ModalContent, ModalHeader, ModalFooter, ModalBody, ModalCloseButton, useDisclosure} from '@chakra-ui/react';
import {FC, useState} from 'react';
import {DeleteIcon, ArrowForwardIcon} from '@chakra-ui/icons';
import {useDeleteTask, useUpdateTaskDescription} from '../hooks/useTasks';

type TaskType = {
    id: number;
    title: string;
    desc: string;
};

interface TaskProps {
    task: TaskType;
}

const Task: FC<TaskProps> = ({task}) => {
    const {mutate} = useDeleteTask();
    const {mutate: updateTaskDescription} = useUpdateTaskDescription();
    const {isOpen, onOpen, onClose} = useDisclosure();
    const [description, setDescription] = useState(task.desc);

    const deleteTask = (id: number) => {
        mutate(id);
    };

    const handleChange = (e: any) => {
        setDescription(e.target.value);
    };

    return (
        <Box onClick={onOpen} draggable cursor="move" id={task.id.toString()} key={task.id} display="flex" justifyContent={'space-between'} alignItems={'center'} bg="white" boxShadow={'2px 2px 2px black'} color="black" height="70px" width="100%" mb="10px" borderRadius={'4px'} padding={'15px'}>
            <Input hidden />
            <Text fontSize={'lg'}>{task.title}</Text>
            <ArrowForwardIcon color="green.400" />

            <Modal isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent minWidth="50vw" minHeight="50vh">
                    <ModalHeader>
                        <Heading size="xl">{task.title}</Heading>
                    </ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Heading size="lg">Description</Heading>
                        <Textarea
                            resize={'none'}
                            display="block"
                            minHeight="30vh"
                            maxHeight={'60vh'}
                            overflow={'scroll'}
                            mt={'20px'}
                            fontSize="2xl"
                            size="lg"
                            value={description}
                            placeholder={`${!description && 'Please enter a description'}`}
                            onChange={handleChange}
                            onBlur={() => updateTaskDescription({id: task.id, title: task.title, desc: description})}
                        />
                    </ModalBody>

                    <ModalFooter display={'flex'} justifyContent={'space-between'}>
                        <IconButton onClick={() => deleteTask(task.id)} aria-label="Delete task" colorScheme={'red'} size="lg" icon={<DeleteIcon />} />
                        <Button colorScheme="blue" mr={3} onClick={onClose}>
                            Close
                        </Button>
                    </ModalFooter>
                </ModalContent>
            </Modal>
        </Box>
    );
};

export default Task;
