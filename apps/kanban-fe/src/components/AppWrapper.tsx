import {QueryClientProvider, QueryClient} from 'react-query';
import {Button, useColorMode, Box, Heading} from '@chakra-ui/react';
import Categories from './Categories';

const queryClient = new QueryClient();

export default function AppWrapper() {
    const {colorMode, toggleColorMode} = useColorMode();
    return (
        <QueryClientProvider client={queryClient}>
            <Box display={'flex'} flexDirection={'column'} justifyContent={'space-between'}>
                <Box display={'flex'} alignItems={'center'} flexDirection={'column'}>
                    <Heading>CamBam</Heading>
                </Box>

                <Categories />

                <Button position={'absolute'} top="0" left="0" onClick={toggleColorMode}>
                    Toggle {colorMode === 'light' ? 'Dark' : 'Light'} Mode
                </Button>
            </Box>
        </QueryClientProvider>
    );
}
