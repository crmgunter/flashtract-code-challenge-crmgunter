import {FormControl, FormLabel, FormErrorMessage, Button, Input, Box, useDisclosure, Heading, IconButton, Modal, ModalOverlay, ModalContent, ModalHeader, ModalFooter, ModalBody, ModalCloseButton} from '@chakra-ui/react';
import {Formik, Form, Field} from 'formik';
import {useCreateCategory} from '../hooks/useCategories';
import {AddIcon} from '@chakra-ui/icons';
import {useRef} from 'react';

const AddCategoryModal = () => {
    const {mutate} = useCreateCategory();
    const {isOpen, onOpen, onClose} = useDisclosure();
    const initialRef = useRef<HTMLInputElement>(null);

    const handleSubmit = (values: {title: string}) => {
        mutate(values);
        onClose();
    };

    return (
        <Box>
            <IconButton aria-label="Add Icon" colorScheme={'blue'} onClick={onOpen} icon={<AddIcon />} />

            <Modal initialFocusRef={initialRef} isOpen={isOpen} onClose={onClose}>
                <ModalOverlay />
                <ModalContent minWidth={'30vw'}>
                    <ModalHeader>
                        <Heading size="lg">Add Category</Heading>
                    </ModalHeader>
                    <ModalCloseButton />
                    <ModalBody>
                        <Box width="100%" display="flex" justifyContent={'center'}>
                            <Formik
                                initialValues={{title: ''}}
                                onSubmit={(values, actions) => {
                                    handleSubmit(values);
                                    actions.resetForm();
                                }}>
                                {(props) => (
                                    <Form style={{width: '100%'}}>
                                        <Box display={'flex'} flexDirection={'column'} justifyContent="center" alignItems={'center'}>
                                            <Field hidden name="title">
                                                {(props: any) => (
                                                    <FormControl isInvalid={props.form.errors.title && props.form.touched.title}>
                                                        <FormLabel fontSize={'large'} htmlFor="title">
                                                            Category Title
                                                        </FormLabel>
                                                        <Input ref={initialRef} required {...props.field} id="title" placeholder="Category Title" />
                                                        <FormErrorMessage>{props.form.errors.title}</FormErrorMessage>
                                                    </FormControl>
                                                )}
                                            </Field>
                                            <Button mt={'5'} type="submit">
                                                Submit
                                            </Button>
                                        </Box>
                                    </Form>
                                )}
                            </Formik>
                        </Box>
                    </ModalBody>

                    <ModalFooter display={'flex'} justifyContent={'space-between'}></ModalFooter>
                </ModalContent>
            </Modal>
        </Box>
    );
};

export default AddCategoryModal;
