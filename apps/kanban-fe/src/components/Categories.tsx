import {Box, Heading} from '@chakra-ui/react';
import {useGetCategories} from '../hooks/useCategories';
import {useUpdateTask} from '../hooks/useTasks';
import Category from './Category';
import {useState} from 'react';
import AddCategoryModal from './AddCategoryModal';

export default function Categories() {
    const categories = useGetCategories('categories', 'http://localhost:3333/api/categories');

    const [draggedCategory, setDraggedCategory] = useState(0);

    const {mutate} = useUpdateTask();

    const handleDrag = (id: number, e: any) => {
        e.dataTransfer.dropEffect = 'move';
        e.preventDefault();
        if (draggedCategory != id) setDraggedCategory(id);
    };

    const handleDrop = (id: number, e: any) => {
        e.preventDefault();
        mutate({id: e.target.id, category: draggedCategory});
    };

    if (categories.isLoading) return <div>Loading...</div>;

    return (
        <Box mt="40px" height="70vh" display="flex" justifyContent={'flex-start'} minWidth="100%" overflowY={'scroll'}>
            {categories.data &&
                categories.data.data.map((category: {id: number; title: string; tasks: []}) => (
                    <Box marginInline={'15px'} onDragEnd={(e) => handleDrop(category.id, e)} onDragOver={(e) => handleDrag(category.id, e)} key={category.id}>
                        <Category category={category} key={category.id} />
                    </Box>
                ))}
            <Box marginInline={'15px'}>
                <Box bg="#335" padding={'15px'} textColor={'white'} minWidth={['50vw', '50vw', '40vw', '20vw']} height="10%" display="flex" alignItems={'center'} justifyContent={'center'} flexDirection={'column'} borderRadius={'8px'}>
                    <Box display={'flex'} justifyContent={'space-between'} alignItems={'center'} width={'100%'}>
                        <Heading as={'h2'} size="lg" whiteSpace={'nowrap'} overflow={'hidden'} textOverflow={'ellipsis'}>
                            Add Category
                        </Heading>
                        <AddCategoryModal />
                    </Box>
                </Box>
            </Box>
        </Box>
    );
}
