# flashtract-code-challenge

## A brief note

I apologize in advance for the long winded README ahead of you... A README is worth a thousand words I suppose.

The below command should get you going

```
yarn install
yarn start
```

The db should seed the first time you spin up the back end server and the categories table is empty. If you run into any troubles with it, let me know and I'll happily undo whatever I did to break it. Or redo whatever I didn't do to prevent it from working. I'm also happy to give a demo if desired.

I also changed the prettier config file to my preferences. Feel free to remove/change to your liking if necessary.

## Functionality

Basic functionality should be as requested, with a few personal tweaks. The app is a basic clone of Trello and heavily relies on Chakra UI for styling. You are able to create Categories and within those categories you can create Tasks that will belong to the category. Each Category has a create Icon button which opens a modal containing a form to perform the POST request. If a task is clicked, a modal opens that displays the title and **editable** description as well as a Button Icon that performs a DELETE request for that task. In reality, I'd display some kind of "Are you sure you want to delete this???" message, but I figured for this challenge it wasn't necessary. Categories can also be deleted, and if deleted, all Tasks it owns are also deleted. Drag and drop movement is implemented and performs a PUT request to change ownership of the Task once the element is dropped into a Category. The backend has full CRUD methods built for each entity. Oh, and just to be cute, I added a dark mode/light mode button toggle. It defaults to dark mode as I have a personal vendetta against light mode.

## Challenges

Most of the technologies, I've never touched. 

I really enjoyed nestJS once I started getting the hang of it and because I'm familiar with MVC frameworks it wasn't terribly hard to fall into the patterns; however, it's been awhile since I've been in SQL world so it took me awhile to get the one-to-many relationship set up. I explored DBeaver to visualize the tables and Postman to test endpoints.

Chakra UI I've also not used, but it was very similar to Material UI so it didn't give me too much trouble. The main issue I had was finding a good "card" component. I didn't see one so I resorted to using Box components with custom styles. 

**I LOVE REACT QUERY.** Another tech I'd never used, but holy cow what a life saver. It was tricky to learn but once I learned about invalidating requests to requery for state updates, I was off to the races. 

Now for Typescript. Wow. That was hard. I was familiar with the concept of Typescript prior to now but had never written it. It feels like an acquired taste and something that becomes second nature over time but starting out made me feel like I was learning how to code all over again. The big things that gave me hiccups were passing props to child components and function/array/object types. I have a much better understanding of it now, but am still picking up things here and there. I can see the benefit of it in a large code base, especially with lots of people, and with more experience. If I were to build this again as a personal project, I'd probably drop typescript just for sake of time and comfort. But I also hear people say that they can't imagine not using Typescript after learning it. Another big issue for me was bringing in libraries and using them the Typescript way. Things that would normally take me 5 minutes took much longer. But again, I imagine that is solved easily once the Typescript instict is in the brain. So. Anyway, I could probably type another 1000 words about how I feel about Typescript but I won't for everyone's sake.

**Update days later:** Okay I get it. I literally saw a situation at work after writing the above where typescript would've been nice. My coworker had me get eyes on an issue he had with a submit button spinner. He was passing a prop as `submitting` instead of `isSubmitting`. Typescript would have stopped that. 

Other very minor bumps were just switching to yarn, gitlab, setting up nx, and node versioning (thanks asdf). Nothing major here, just had to adjust over to slighty different setups. Nothing much to write home about.

## Knowing when to stop

An additional challenge I figured I'd mention was that I found myself continually trying to build features that were completely unnecessary for the task. I'm genuinely having fun with this project and have many ideas on how to improve on it including:

* Adding options for task status (urgent, priority, low priority)
* Editable content fields
* Color coding cards
* Moveable categories
* Archived tasks
* Keeping track of task order in list

As of 8pm though, I have decided to add these to a metaphorical backlog.

## Build process

I started with just learning nestJS and exploring their docs to understand how to use it. Pretty similar to stuff I've used in the past, and I found their CLI commands helpful. I set up the entities, controllers, services, etc and used Postman to verify the endpoints were working and DBeaver as a SQL GUI. Once that was set up, I made some basic axios calls on the Front End to get back data. After that was kosher, I moved on to requests with react query and learned about invalidating. Once I had state changing instantly on POST, I added DELETE and PUT queries. With that all done, I moved on to styling and design. Initially I just had some inputs at the top of the page to create categories and tasks but decided to make it look a little more like Trello. This actually solved a relationship issue I had. I initially was defaulting the tasks to be created under the first category in the db, but allowing them to be created under individual categories allowed me to relate them to specific categories. Then for moving the tasks around, I actually went straight to drag and drop. It felt like the most natural and easiest way to handle the movement. It was tricky, but not impossible. I added Formik for forms, and would usually incorporate yup validation but didn't in this case. At that point, with the bulk of work done, I went back and made descriptions editable, and then went down the rabbit hole of seeding the database. This was incredibly challenging for me with limited knowledge of nestJS. I ultimately decided to seed it on server start up if the categories table was empty. I know this is probably not the ideal way to seed it but it was the way I found after devouring docs and youtube videos. Aside from that, it was just continual refactoring, tweaking, improving features, and styling.

## Resources

Pretty straight forward here. If the tech had documentation, I was swimming in it. I also used a ton of stack overflow (imagine that) and youtube videos.

## Conclusion

If you've made it this far, thanks for reading!

![The End](https://media0.giphy.com/media/l4FAPaGGeB7D1LfIA/giphy.gif)